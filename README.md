# spring-boot-elk

Exemplo de API + ELK (Elasticsearch, Logstash e Kibana)

## Tecnologias utilizadas

* Java, versão: 8
* Maven
  * Spring Boot, versão: 2.1.14.RELEASE
  * Spring boot devtools
  * H2 Database
